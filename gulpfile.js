const gulp = require('gulp');
const path = require('path');
const watch = require('gulp-watch');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const ngAnnotate = require('gulp-ng-annotate');

const rootDir = './public';

gulp.task('run-dev', () => {
    return watch('./src/**/*.js', () => {
        const now = new Date();
        console.log('Latest build: ' + now.getDate() + '-' + now.getMonth() + '-' + now.getFullYear() + ', ' + now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds());

        gulp.src(['./src/Startup/*.js', './src/Controller/*.js', './src/Service/*.js'])
            .pipe(concat({ path: 'index.js' }))
            .pipe(ngAnnotate())
            .pipe(babel({ presets: ['es2015'] }))
            .pipe(gulp.dest('./public/js'));
    });
});

gulp.task('production', () => {
    gulp.src(['./public/js/index.js'])
        .pipe(uglify())
        .pipe(rename('index.min.js'))
        .pipe(gulp.dest('./public/js'));
});