'use strict';

// Initialize Angular
angular.module('app', ['ngRoute']);
angular.module('app').config(["$routeProvider", "$locationProvider", function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider.when('/', {
        templateUrl: '/partial/Login.html',
        controller: 'LoginController'
    }).when('/Customer', {
        templateUrl: '/partial/Customer/index.html'
    }).when('/buyer', {
        templateUrl: '/partial/buyer.html',
        controller: 'BuyingController'
    }).when('/Agent', {
        templateUrl: '/partial/Agent/index.html',
        controller: 'AgentController'
    });
}]);
angular.module('app').controller('AgentController', ["$scope", function ($scope) {

    $scope.launchModal = function () {
        $('.ui.modal.bid').modal({
            closable: false,
            onVisible: function onVisible() {
                return $('#range-2').range({ min: 0, max: 2, start: 1, input: '#input-2' });
            },
            onApprove: function onApprove() {
                // $('#card-1').remove();
                $('.mainbutton').addClass('disabled')
            }
        }).modal('show');
    };
}]);
angular.module('app').controller('BuyingController', ["$scope", "BuyingService", function ($scope, BuyingService) {

    // $scope.$on('$viewContentLoaded', function () {
    // alert('loaded');
    // console.log('calling mapInit()');
    // var platform = new H.service.Platform({
    //   'app_id': 'd05pqp7kw1Dh904XgSdw',
    //   'app_code': 'kOQ1p3PSWQxb0m63Z5_QUg'
    // }); 
    // Obtain the default map types from the platform object:
    // var defaultLayers = platform.createDefaultLayers();

    // Instantiate (and display) a map object:
    //   var map = new H.Map( 
    //     document.getElementById('map'),
    //     defaultLayers.normal.map,
    //     {
    //       zoom: 10,
    //       center: { lat: $scope.sgCentralX, lng: $scope.sgCentralY }
    //     }); 
    //   console.log('supposedly done loading');

    // });  
    // $scope.pinLocation = 'Clementi';
    // $scope.sgCentralX = 1.36666667;
    // $scope.sgCentralX = 103.8; 
    // $scope.mapSizeX = '640px';
    // $scope.mapSizeY = '480px';
    // $scope.hereAppId = 'd05pqp7kw1Dh904XgSdw';
    // $scope.hereAppCode = 'kOQ1p3PSWQxb0m63Z5_QUg';
    // $scope.mapInit = function () {
    //   console.log('calling mapInit()');
    //   var platform = new H.service.Platform({
    //     'app_id': $scope.hereAppId,
    //     'app_code': $scope.hereAppCode
    //   });
    //   // Obtain the default map types from the platform object:
    //   var defaultLayers = platform.createDefaultLayers();

    //   // Instantiate (and display) a map object:
    //   var map = new H.Map(
    //     document.getElementById('map'),
    //     defaultLayers.normal.map,
    //     {
    //       zoom: 10,
    //       center: { lat: $scope.sgCentralX, lng: $scope.sgCentralY }
    //     });
    // }
}]);
angular.module('app').controller('LoginController', ["$scope", "$location", function ($scope, $location) {

    $scope.email = '';
    $scope.onLogin = function () {
        if ($scope.email == 'homesgadmin@gmail.com') $location.path('/Agent');else $location.path('/Customer');
    };
}]);
angular.module('app').controller('MenuController', ["$scope", "$route", function ($scope, $route) {

    $scope.$on('$locationChangeStart', function () {
        $scope.$applyAsync(function () {
            $scope.currentTab = $route.current.activeTab;
        });
    });
}]);
angular.module('app').service('BuyingService', function () {});