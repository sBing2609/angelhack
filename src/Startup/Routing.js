angular.module('app').config(function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider
        .when('/', {
            templateUrl: '/partial/Login.html',
            controller: 'LoginController'
        })
        .when('/Customer', {
            templateUrl: '/partial/Customer/index.html'
        })
        .when('/buyer', {
            templateUrl: '/partial/buyer.html',
            controller: 'BuyingController'
        }) 
        .when('/Agent', {
            templateUrl: '/partial/Agent/index.html',
            controller: 'AgentController'
        });
        
});