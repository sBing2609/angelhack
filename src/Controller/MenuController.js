angular.module('app').controller('MenuController', function($scope, $route) {

    $scope.$on('$locationChangeStart', function () {
        $scope.$applyAsync(function () {
            $scope.currentTab = $route.current.activeTab;
        });
    });

});