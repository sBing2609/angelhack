angular.module('app').controller('LoginController', function ($scope, $location) {

    $scope.email = '';
    $scope.onLogin = () => {
        if ($scope.email == 'homesgadmin@gmail.com')
            $location.path('/Agent');
        else
            $location.path('/Customer');
    }

});