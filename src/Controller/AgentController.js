angular.module('app').controller('AgentController', function ($scope) {

    $scope.launchModal = () => {
        $('.ui.modal')
            .modal({
                closable: false,
                onVisible: () => $('#range-2').range({ min: 0, max: 2, start: 1, input: '#input-2' }),
                onApprove: () => {
                    //$('#card-1').remove();
                }
            })
            .modal('show');
    }


});